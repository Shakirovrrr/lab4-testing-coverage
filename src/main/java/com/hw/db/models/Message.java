package com.hw.db.models;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Objects;

public class Message {
	private String message;
	private Object data;

	@JsonCreator
	public Message(
			@JsonProperty("message") Enum<?> message,
			@JsonProperty("data") Object data
	) {
		this.message = message.toString();
		this.data = data;
	}

	@JsonCreator
	public Message(@JsonProperty("message") Enum<?> message) {
		this.message = message.toString();
		this.data = null;
	}

	@JsonCreator
	public Message(@JsonProperty("data") Object data) {
		this.message = null;
		this.data = data;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Message message1 = (Message) o;
		return Objects.equals(message, message1.message) && Objects.equals(data, message1.data);
	}

	@Override
	public int hashCode() {
		return Objects.hash(message, data);
	}
}



