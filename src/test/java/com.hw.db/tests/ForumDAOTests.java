package com.hw.db.tests;

import com.hw.db.DAO.ForumDAO;
import com.hw.db.DAO.ThreadDAO;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;

public class ForumDAOTests {

	@Test
	@DisplayName("User gets list of threads test #1")
	void threadListTest1() {
		JdbcTemplate mockJdbc = Mockito.mock(JdbcTemplate.class);
		ForumDAO forum = new ForumDAO(mockJdbc);
		ThreadDAO.ThreadMapper THREAD_MAPPER = new ThreadDAO.ThreadMapper();
		ForumDAO.ThreadList("slug", null, null, null);
		Mockito.verify(mockJdbc).query(Mockito.eq("SELECT * FROM threads WHERE forum = (?)::CITEXT ORDER BY created;"), Mockito.any(Object[].class), Mockito.any(ThreadDAO.ThreadMapper.class));
	}

}
