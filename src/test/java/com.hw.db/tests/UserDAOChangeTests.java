package com.hw.db.tests;

import com.hw.db.DAO.UserDAO;
import com.hw.db.models.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;

public class UserDAOChangeTests {
	private User user;
	private JdbcTemplate jdbc;
	private UserDAO dao;

	@BeforeEach
	void clearUser() {
		user = new User("User1", null, null, null);

		jdbc = Mockito.mock(JdbcTemplate.class);
		dao = new UserDAO(jdbc);
	}

	@Test
	@DisplayName("Do not change anything")
	void changeNothing() {
		UserDAO.Change(user);

		Mockito.verify(jdbc, Mockito.never()).update(Mockito.anyString(), Mockito.anyCollection());
	}

	@Test
	@DisplayName("Change user email")
	void testChangeEmail() {
		user.setEmail("user1@example.com");
		UserDAO.Change(user);

		Mockito.verify(jdbc).update("UPDATE \"users\" SET  email=?  WHERE nickname=?::CITEXT;", user.getEmail(), user.getNickname());
	}

	@Test
	@DisplayName("Change user full name")
	void testChangeFullName() {
		user.setFullname("User Userovich");
		UserDAO.Change(user);

		Mockito.verify(jdbc).update("UPDATE \"users\" SET  fullname=?  WHERE nickname=?::CITEXT;", user.getFullname(), user.getNickname());
	}

	@Test
	@DisplayName("Change user about")
	void testChangeAbout() {
		user.setAbout("Some info");
		UserDAO.Change(user);

		Mockito.verify(jdbc).update("UPDATE \"users\" SET  about=?  WHERE nickname=?::CITEXT;", user.getAbout(), user.getNickname());
	}

	@Test
	@DisplayName("Change everything except about")
	void testChangeAllExceptAbout() {
		user = new User("User1", "user1@example.com", "User Userovich", null);
		UserDAO.Change(user);

		Mockito.verify(jdbc).update(
			"UPDATE \"users\" SET  email=? , fullname=?  WHERE nickname=?::CITEXT;",
			user.getEmail(), user.getFullname(), user.getNickname()
		);
	}

	@Test
	@DisplayName("Change everything except fullanme")
	void testChangeAllExceptFullname() {
		user = new User("User1", "user1@example.com", null, "Some info");
		UserDAO.Change(user);

		Mockito.verify(jdbc).update(
			"UPDATE \"users\" SET  email=? , about=?  WHERE nickname=?::CITEXT;",
			user.getEmail(), user.getAbout(), user.getNickname()
		);
	}

	@Test
	@DisplayName("Change everything except email")
	void testChangeAllExceptEmail() {
		user = new User("User1", null, "User Userovich", "Some info");
		UserDAO.Change(user);

		Mockito.verify(jdbc).update(
			"UPDATE \"users\" SET  fullname=? , about=?  WHERE nickname=?::CITEXT;",
			user.getFullname(), user.getAbout(), user.getNickname()
		);
	}

	@Test
	@DisplayName("Change everything")
	void testChangeAll() {
		user = new User("User1", "user1@example.com", "User Userovich", "Some info");
		UserDAO.Change(user);

		Mockito.verify(jdbc).update(
			"UPDATE \"users\" SET  email=? , fullname=? , about=?  WHERE nickname=?::CITEXT;",
			user.getEmail(), user.getFullname(), user.getAbout(), user.getNickname()
		);
	}

}
