package com.hw.db.tests;

import com.hw.db.DAO.ThreadDAO;
import com.hw.db.models.Post;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.List;

public class ThreadDAOTreeSortTests {
	private final int since = (int) System.currentTimeMillis();
	private final int limit = 42;
	private final int id = 1;

	@Test
	void treeSortTestDescTrue() {
		final JdbcTemplate jdbc = Mockito.mock(JdbcTemplate.class);
		final ThreadDAO dao = new ThreadDAO(jdbc);

		List<Post> result = ThreadDAO.treeSort(id, limit, since, true);
		Assertions.assertEquals(List.of(), result);
	}

	@Test
	void treeSortTestDescFalse() {
		final JdbcTemplate jdbc = Mockito.mock(JdbcTemplate.class);
		final ThreadDAO dao = new ThreadDAO(jdbc);

		List<Post> result = ThreadDAO.treeSort(id, limit, since, false);
		Assertions.assertEquals(List.of(), result);
	}
}
