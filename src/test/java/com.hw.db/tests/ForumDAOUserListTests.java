package com.hw.db.tests;

import com.hw.db.DAO.ForumDAO;
import com.hw.db.models.User;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.List;

public class ForumDAOUserListTests {
	private final String slug = "Slug1";
	private final int limit = 42;
	private final String since = String.valueOf(System.currentTimeMillis());

	@Test
	void userListTest1() {
		JdbcTemplate jdbc = Mockito.mock(JdbcTemplate.class);
		ForumDAO dao = new ForumDAO(jdbc);

		List<User> result = ForumDAO.UserList(slug, limit, since, true);
		Assertions.assertEquals(List.of(), result);
	}

	@Test
	void userListTest2() {
		JdbcTemplate jdbc = Mockito.mock(JdbcTemplate.class);
		ForumDAO dao = new ForumDAO(jdbc);

		List<User> result = ForumDAO.UserList(slug, limit, since, null);
		Assertions.assertEquals(List.of(), result);
	}

	@Test
	void userListTest3() {
		JdbcTemplate jdbc = Mockito.mock(JdbcTemplate.class);
		ForumDAO dao = new ForumDAO(jdbc);

		List<User> result = ForumDAO.UserList(slug, null, null, false);
		Assertions.assertEquals(List.of(), result);
	}
}
